/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt_arbol.bst;

/**
 *
 * @author ronal
 */
public class Nodo {
    private int data;
    private Nodo HI;
    private Nodo HD;

    public Nodo(){
        this.data = 0;
        this.HI = null;
        this.HD = null;
    }
    
    public Nodo(int data) {
        this.data = data;
        this.HI = null;
        this.HD = null;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public Nodo getHI() {
        return HI;
    }

    public void setHI(Nodo HI) {
        this.HI = HI;
    }

    public Nodo getHD() {
        return HD;
    }

    public void setHD(Nodo HD) {
        this.HD = HD;
    }
    
    public int cantidadHijos() {
        int c = 0;
        if(HI != null)
            c++;
        if(HD != null)
            c++;
        return c;
    }
    
    public boolean esHoja() {
        return cantidadHijos() == 0;
    }

    @Override
    public String toString() {
        return " ( " + data + " ) ";
    }
    
}
