/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adt_arbol.bst;

/**
 *
 * @author ronal
 */
public class Main {

    private static int data[] = {75, 50, 100, 25, 70, 10, 111, 80};
    
    public static void main(String[] args) {
        
        Arbol bst = new Arbol();
        load(bst);
        
        imprimir(bst, "MOSTRANDO INORDEN");        
        /*bst.podar(100);
        imprimir(bst, "DESPUES DE PODAR 100");
        
        bst.eliminar(75);
        imprimir(bst, "DESPUES DE ELIMINAR 75");
        
        System.out.println("Nivel : " + bst.nivel());
        
        bst.podarNivel(3);
        imprimir(bst, "DESPUES DE PODAR POR NIVEL 3");
        bst.eliminarNivel(3);
        imprimir(bst, "DESPUES DE ELIMINAR POR NIVEL 3");*/
        
        bst.eliminarTriangulo();
        imprimir(bst, "DESPUES DE ELIMINAR TRIANGULO IMPAR");
                
    }
    
    private static void load(Arbol bst) {
        for (int x : data) {
            bst.insertar(x);
        }
    }
    
    private static void imprimir(Arbol bst, String titulo) {
        System.out.println(titulo);
        bst.inOrden();
        System.out.println("\n");
    }
    
}
